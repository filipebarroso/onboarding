class AppStrings {
  // General
  static const appTitle = 'Onboarding';
  static const next = 'Next';
  static const save = 'Save';

  // Home
  static homeTitle(String name) => 'Hello $name, Welcome back';
  static const homePleaseOnboard = 'Please Onboard';
  static const homeOnboardLabel = 'Onboard';

  // Name
  static const nameTitle = 'Welcome onboard!';
  static const nameSubtitle = 'First off, what should I call you?';
  static const nameNameLabel = 'Your name';

  static const nameNameLabelError = 'Please enter a name';
  static const nameNameLabelErrorLength =
      'Please your name must have at least 2 character length';
  static const nameNameLabelErrorNumbers = 'Your name cannot include numbers';

  // Age
  static const ageTitle = 'How old are you?';
}
