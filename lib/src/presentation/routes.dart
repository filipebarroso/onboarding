import 'package:flutter/material.dart';

const String routeHome = '/';
const String routeName = '/onboard-name';
const String routeAge = '/onboard-age';

class Navigation {
  static Future<void> navigateHome(BuildContext context) {
    return Navigator.of(context).pushReplacementNamed(routeHome);
  }

  static Future<void> navigateOnboardName(BuildContext context) {
    return Navigator.of(context).pushNamed(routeName);
  }

  static Future<void> navigateOnboardAge(BuildContext context) {
    return Navigator.of(context).pushNamed(routeAge);
  }
}
