import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:onboarding/src/application/onboard/onboard_bloc.dart';
import 'package:onboarding/src/presentation/assets/dimensions.dart';
import 'package:onboarding/src/presentation/assets/strings.dart';
import 'package:onboarding/src/presentation/routes.dart';
import 'package:onboarding/src/presentation/widgets/app_text_form_field.dart';
import 'package:onboarding/src/presentation/widgets/primary_button.dart';
import 'package:onboarding/src/presentation/widgets/subittle.dart';
import 'package:onboarding/src/presentation/widgets/title.dart';

class OnboardNameScreen extends StatefulWidget {
  const OnboardNameScreen({Key? key}) : super(key: key);

  @override
  State<OnboardNameScreen> createState() => _OnboardNameScreenState();
}

class _OnboardNameScreenState extends State<OnboardNameScreen> {
  final _formKey = GlobalKey<FormState>();

  final TextEditingController _textController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: Dimensions.medium,
          vertical: Dimensions.small,
        ),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Spacer(),
              const TitleWidget(title: AppStrings.nameTitle),
              const SizedBox(height: Dimensions.mini),
              const Subtitle(label: AppStrings.nameSubtitle),
              const SizedBox(height: Dimensions.medium),
              AppTextFormFiled(textController: _textController),
              const Spacer(),
              PrimaryButton(
                label: AppStrings.next,
                onPressed: () {
                  if (_formKey.currentState!.validate()) {
                    // Save name
                    final name = _textController.value.text;
                    context.read<OnboardBloc>().add(OnboardSaveNameEvent(name));

                    // Navigate to next screen
                    Navigation.navigateOnboardAge(context);
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _textController.dispose();
    super.dispose();
  }
}
