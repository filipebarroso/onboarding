import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:onboarding/src/application/onboard/onboard_bloc.dart';
import 'package:onboarding/src/application/user/user_bloc.dart';
import 'package:onboarding/src/domain/models/user_model.dart';
import 'package:onboarding/src/presentation/assets/constants.dart';
import 'package:onboarding/src/presentation/assets/dimensions.dart';
import 'package:onboarding/src/presentation/assets/strings.dart';
import 'package:onboarding/src/presentation/routes.dart';
import 'package:onboarding/src/presentation/widgets/age_chip.dart';
import 'package:onboarding/src/presentation/widgets/app_bar.dart';
import 'package:onboarding/src/presentation/widgets/primary_button.dart';
import 'package:onboarding/src/presentation/widgets/title.dart';

class OnboardAgeRangeScreen extends StatefulWidget {
  const OnboardAgeRangeScreen({Key? key}) : super(key: key);

  @override
  State<OnboardAgeRangeScreen> createState() => _OnboardAgeRangeScreenState();
}

class _OnboardAgeRangeScreenState extends State<OnboardAgeRangeScreen> {
  @override
  Widget build(BuildContext context) {
    final validInput = context.read<OnboardBloc>().state.isValid();
    final pickedRange = context.read<OnboardBloc>().state.range;
    return Scaffold(
      appBar: ExerciseAppBar(),
      body: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: Dimensions.medium,
          vertical: Dimensions.small,
        ),
        child: BlocBuilder<OnboardBloc, OnboardState>(
          buildWhen: (previous, current) => previous.range != current.range,
          builder: (context, state) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const TitleWidget(
                  title: AppStrings.ageTitle,
                ),
                const SizedBox(
                  height: Dimensions.medium,
                ),
                Expanded(
                  child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: ageRange.length,
                    itemBuilder: (context, index) {
                      return Row(
                        children: [
                          Padding(
                            padding: EdgeInsets.only(
                              top: index > 0 ? Dimensions.small : 0,
                            ),
                            child: AgeChip(
                              enabled: index == pickedRange,
                              label: ageRange[index],
                              onPressed: () {
                                setState(
                                  () {
                                    if (pickedRange == index) {
                                      context.read<OnboardBloc>().add(
                                            const OnboardClearRangeEvent(),
                                          );
                                    } else {
                                      context.read<OnboardBloc>().add(
                                            OnboardSaveRangeEvent(index),
                                          );
                                    }
                                  },
                                );
                              },
                            ),
                          ),
                        ],
                      );
                    },
                  ),
                ),
                PrimaryButton(
                  label: AppStrings.save,
                  onPressed: validInput
                      ? () {
                          _saveUser(context);
                          Navigation.navigateHome(context);
                        }
                      : null,
                ),
              ],
            );
          },
        ),
      ),
    );
  }

  void _saveUser(BuildContext context) {
    final userName = context.read<OnboardBloc>().state.name ?? '';
    final userAgeRange =
        context.read<OnboardBloc>().state.range ?? 0; // <- Problematic

    context.read<UserBloc>().add(
          UserSaveEvent(
            UserModel(
              name: userName,
              range: userAgeRange,
            ),
          ),
        );
  }
}
