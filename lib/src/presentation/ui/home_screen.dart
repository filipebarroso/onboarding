import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:onboarding/src/application/onboard/onboard_bloc.dart';
import 'package:onboarding/src/application/user/user_bloc.dart';
import 'package:onboarding/src/presentation/assets/dimensions.dart';
import 'package:onboarding/src/presentation/assets/strings.dart';
import 'package:onboarding/src/presentation/routes.dart';
import 'package:onboarding/src/presentation/widgets/primary_button.dart';
import 'package:onboarding/src/presentation/widgets/title.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    context.read<UserBloc>().add(UserLoadEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: Dimensions.medium,
          vertical: Dimensions.small,
        ),
        child: BlocBuilder<UserBloc, UserState>(
          builder: (context, state) {
            final user = state.user;
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Spacer(),
                if (user != null)
                  TitleWidget(
                    title: AppStrings.homeTitle(user.name),
                  )
                else
                  const TitleWidget(title: AppStrings.homePleaseOnboard),
                const Spacer(),
                PrimaryButton(
                  label: AppStrings.homeOnboardLabel,
                  onPressed: () {
                    context.read<OnboardBloc>().add(OnboardClearEvent());
                    Navigation.navigateOnboardName(context);
                  },
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
