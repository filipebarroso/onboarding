import 'package:flutter/material.dart';

class AgeChip extends StatelessWidget {
  const AgeChip({
    Key? key,
    required this.label,
    required this.onPressed,
    required this.enabled,
  }) : super(key: key);

  final String label;
  final Function() onPressed;
  final bool enabled;

  @override
  Widget build(BuildContext context) {
    final text = Theme.of(context).textTheme.bodyText1;
    return ActionChip(
      label: Text(label),
      onPressed: onPressed,
      backgroundColor:
          enabled ? const Color(0xFF5413D8) : const Color(0xFFEBE6FF),
      labelStyle: enabled
          ? text?.copyWith(color: Colors.white)
          : text?.copyWith(color: const Color(0xFF5413D8)),
    );
  }
}
