import 'package:flutter/material.dart';
import 'package:onboarding/src/presentation/assets/dimensions.dart';

/// This is a wrapper for the [ElevatedButton] to give it more space around it
///
/// example:
/// ```
///
/// ```
class PrimaryButton extends StatelessWidget {
  const PrimaryButton({
    Key? key,
    required this.label,
    required this.onPressed,
  }) : super(key: key);

  final String label;
  final Function()? onPressed;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: Dimensions.medium,
        vertical: Dimensions.small,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Expanded(
            child: ElevatedButton(
              onPressed: onPressed,
              child: Text(label),
            ),
          ),
        ],
      ),
    );
  }
}
