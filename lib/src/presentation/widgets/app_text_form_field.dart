import 'package:flutter/material.dart';
import 'package:onboarding/src/presentation/assets/strings.dart';

class AppTextFormFiled extends StatelessWidget {
  const AppTextFormFiled({
    Key? key,
    required TextEditingController textController,
  })  : _textController = textController,
        super(key: key);

  final TextEditingController _textController;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          AppStrings.nameNameLabel,
          style: Theme.of(context).textTheme.overline,
        ),
        Container(
          padding: const EdgeInsets.all(14.0),
          decoration: BoxDecoration(
            color: const Color(0xFFF0EFF1),
            border: Border.all(
              color: Colors.black,
            ),
            borderRadius: BorderRadius.circular(12.0),
          ),
          child: TextFormField(
            decoration: const InputDecoration(
              border: InputBorder.none,
            ),
            controller: _textController,
            keyboardType: TextInputType.text,
            validator: (value) {
              // Needs to input something
              if (value == null || value.isEmpty) {
                return AppStrings.nameNameLabelError;
              }

              // Minimum length 2
              // Example is 'Lu'
              if (value.length < 2) {
                return AppStrings.nameNameLabelErrorLength;
              }

              // Should be alphabetic only
              if (RegExp(r'[0-9]').hasMatch(value)) {
                return AppStrings.nameNameLabelErrorNumbers;
              }

              return null;
            },
          ),
        ),
      ],
    );
  }
}
