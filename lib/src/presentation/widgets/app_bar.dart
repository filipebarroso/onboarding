import 'package:flutter/material.dart';

class ExerciseAppBar extends AppBar {
  ExerciseAppBar({Key? key})
      : super(
          key: key,
          surfaceTintColor: Colors.white,
        );
}
