import 'package:flutter/material.dart';
import 'package:onboarding/src/presentation/assets/strings.dart';
import 'package:onboarding/src/presentation/routes.dart';
import 'package:onboarding/src/presentation/ui/home_screen.dart';
import 'package:onboarding/src/presentation/ui/onboard_age_screen.dart';
import 'package:onboarding/src/presentation/ui/onboard_name_screen.dart';

class OnboardingApp extends StatelessWidget {
  const OnboardingApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: AppStrings.appTitle,
      theme: ThemeData(
        // Font
        fontFamily: 'Open Sans',
        // App Bar
        appBarTheme: const AppBarTheme(
          elevation: 0,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(
            color: Color(0xFF050506),
          ),
        ),
        // Chip
        chipTheme: const ChipThemeData(
          labelStyle: TextStyle(
            color: Colors.white,
          ),
          backgroundColor: Color(0xFF5413D8),
          padding: EdgeInsets.symmetric(
            vertical: 14.0,
            horizontal: 24.0,
          ),
        ),
        // Elevated Button
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(
            primary: const Color(0xFF5413D8),
            onPrimary: Colors.white,
          ),
        ),
      ),
      initialRoute: routeHome,
      routes: {
        routeHome: (context) => const HomeScreen(),
        routeAge: (context) => const OnboardAgeRangeScreen(),
        routeName: (context) => const OnboardNameScreen(),
      },
    );
  }
}
