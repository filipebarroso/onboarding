part of 'onboard_bloc.dart';

class OnboardState extends Equatable {
  const OnboardState({
    this.name,
    this.range,
  });

  final String? name;
  final int? range;

  OnboardState copyWith({
    String? name,
    int? range,
  }) {
    return OnboardState(
      name: name ?? this.name,
      range: range ?? this.range,
    );
  }

  OnboardState clearAge() {
    return OnboardState(
      name: name,
      range: null,
    );
  }

  OnboardState clear() {
    return const OnboardState(
      name: null,
      range: null,
    );
  }

  bool isValid() {
    return (name?.isNotEmpty ?? false) && (range != null);
  }

  @override
  List<Object?> get props => [
        name,
        range,
      ];
}
