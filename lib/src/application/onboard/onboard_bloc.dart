import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'onboard_event.dart';
part 'onboard_state.dart';

class OnboardBloc extends Bloc<OnboardEvent, OnboardState> {
  OnboardBloc() : super(const OnboardState()) {
    on<OnboardClearEvent>(_onboardClearEvent);
    on<OnboardSaveNameEvent>(_onboardSaveNameEvent);
    on<OnboardSaveRangeEvent>(_onboardSaveRangeEvent);
    on<OnboardClearRangeEvent>(_onboardClearRangeEvent);
  }

  FutureOr<void> _onboardClearEvent(
    OnboardClearEvent event,
    Emitter<OnboardState> emit,
  ) async =>
      emit(
        state.clear(),
      );

  FutureOr<void> _onboardSaveNameEvent(
    OnboardSaveNameEvent event,
    Emitter<OnboardState> emit,
  ) async =>
      emit(
        state.copyWith(
          name: event.name,
        ),
      );

  FutureOr<void> _onboardSaveRangeEvent(
    OnboardSaveRangeEvent event,
    Emitter<OnboardState> emit,
  ) async =>
      emit(
        state.copyWith(
          range: event.range,
        ),
      );

  FutureOr<void> _onboardClearRangeEvent(
    OnboardClearRangeEvent event,
    Emitter<OnboardState> emit,
  ) async =>
      emit(
        state.clearAge(),
      );
}
