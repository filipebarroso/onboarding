part of 'onboard_bloc.dart';

abstract class OnboardEvent extends Equatable {
  const OnboardEvent();
}

class OnboardClearEvent extends OnboardEvent {
  @override
  List<Object?> get props => [];
}

class OnboardSaveNameEvent extends OnboardEvent {
  final String name;

  const OnboardSaveNameEvent(this.name);

  @override
  List<Object?> get props => [name];
}

class OnboardSaveRangeEvent extends OnboardEvent {
  final int range;

  const OnboardSaveRangeEvent(this.range);

  @override
  List<Object?> get props => [range];
}

class OnboardClearRangeEvent extends OnboardEvent {
  const OnboardClearRangeEvent();

  @override
  List<Object?> get props => [];
}
