import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:onboarding/src/domain/models/user_model.dart';
import 'package:onboarding/src/domain/repository/base_user_repository.dart';

part 'user_event.dart';
part 'user_state.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  UserBloc(this._repository) : super(const UserState()) {
    on<UserLoadEvent>(_userLoadEvent);
    on<UserSaveEvent>(_userSaveEvent);
  }

  final BaseUserRepository _repository;

  FutureOr<void> _userLoadEvent(
    UserLoadEvent event,
    Emitter<UserState> emit,
  ) async {
    final user = await _repository.getUser();
    emit(state.copyWith(user: user));
  }

  FutureOr<void> _userSaveEvent(
    UserSaveEvent event,
    Emitter<UserState> emit,
  ) async {
    final user = event.user;
    _repository.saveUser(user);
    emit(state.copyWith(user: user));
  }
}
