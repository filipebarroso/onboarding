part of 'user_bloc.dart';

class UserState extends Equatable {
  const UserState({this.user});

  final UserModel? user;

  UserState copyWith({
    UserModel? user,
  }) {
    return UserState(
      user: user ?? user,
    );
  }

  @override
  List<Object?> get props => [user];
}
