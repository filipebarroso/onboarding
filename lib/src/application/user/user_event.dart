part of 'user_bloc.dart';

abstract class UserEvent extends Equatable {
  const UserEvent();
}

class UserLoadEvent extends UserEvent {
  @override
  List<Object?> get props => [];
}

class UserSaveEvent extends UserEvent {
  const UserSaveEvent(this.user);

  final UserModel user;

  @override
  List<Object?> get props => [user];
}
