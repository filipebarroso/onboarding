import 'package:onboarding/src/data/entities/user.dart';

abstract class UserProvider {
  Future<User?> getUser();
  Future<bool> saveUser(User user);
}
