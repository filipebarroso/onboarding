import 'dart:convert';

import 'package:onboarding/src/data/entities/user.dart';
import 'package:onboarding/src/data/providers/user_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LocalUserProvider extends UserProvider {
  LocalUserProvider();

  Future<SharedPreferences> loadPreferences() async {
    return await SharedPreferences.getInstance();
  }

  final Future<SharedPreferences> _preferences =
      SharedPreferences.getInstance();

  @override
  Future<User?> getUser() async {
    final SharedPreferences local = await _preferences;

    final raw = local.getString('user');
    if (raw == null) {
      return null;
    }
    Map<String, dynamic> json = jsonDecode(raw);
    return User.fromJson(json);
  }

  @override
  Future<bool> saveUser(User user) async {
    final SharedPreferences local = await _preferences;

    Map<String, dynamic> json = user.toJson();
    String raw = jsonEncode(json);

    return local.setString('user', raw);
  }
}
