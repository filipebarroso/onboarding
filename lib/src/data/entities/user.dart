import 'package:flutter/cupertino.dart';
import 'package:onboarding/src/domain/models/user_model.dart';
import 'package:onboarding/src/presentation/assets/constants.dart';

class User {
  User({
    required this.name,
    required this.range,
  });

  String name;
  String range;

  factory User.fromJson(Map<String, dynamic> parsedJson) {
    return User(
        name: parsedJson['name'] ?? "", range: parsedJson['range'] ?? "");
  }

  Map<String, dynamic> toJson() {
    return {
      "name": name,
      "range": range,
    };
  }
}

extension UserToModel on User {
  UserModel toModel() {
    final age = ageRange.indexOf(range);
    if (age < 0) {
      throw ErrorDescription('Invalid Age Range');
    }
    return UserModel(
      name: name,
      range: age,
    );
  }
}
