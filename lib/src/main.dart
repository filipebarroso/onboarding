import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:onboarding/src/application/onboard/onboard_bloc.dart';
import 'package:onboarding/src/application/user/user_bloc.dart';
import 'package:onboarding/src/data/providers/local_user_provider.dart';
import 'package:onboarding/src/domain/repository/user_repository.dart';
import 'package:onboarding/src/presentation/app.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  final userProvider = LocalUserProvider();

  runApp(
    MultiRepositoryProvider(
      providers: [
        RepositoryProvider<UserRepository>(
          create: (_) => UserRepository(userProvider),
        )
      ],
      child: MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (context) =>
                UserBloc(RepositoryProvider.of<UserRepository>(context)),
          ),
          BlocProvider(
            create: (context) => OnboardBloc(),
          ),
        ],
        child: const OnboardingApp(),
      ),
    ),
  );
}
