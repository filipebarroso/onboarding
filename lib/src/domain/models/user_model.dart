import 'package:onboarding/src/data/entities/user.dart';
import 'package:onboarding/src/presentation/assets/constants.dart';

class UserModel {
  UserModel({
    required this.name,
    required this.range,
  });

  final String name;
  final int range;
}

extension UserModelExtensions on UserModel {
  User toUser() {
    return User(
      name: name,
      range: ageRange[range],
    );
  }
}
