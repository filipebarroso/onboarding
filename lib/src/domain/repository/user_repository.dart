import 'package:onboarding/src/data/entities/user.dart';
import 'package:onboarding/src/data/providers/user_provider.dart';
import 'package:onboarding/src/domain/models/user_model.dart';
import 'package:onboarding/src/domain/repository/base_user_repository.dart';

class UserRepository extends BaseUserRepository {
  UserRepository(this._provider);

  final UserProvider _provider;

  @override
  Future<UserModel?> getUser() async {
    final user = await _provider.getUser();
    return user?.toModel();
  }

  @override
  Future<bool> saveUser(UserModel user) async {
    return _provider.saveUser(user.toUser());
  }
}
