import 'package:onboarding/src/domain/models/user_model.dart';

abstract class BaseUserRepository {
  Future<UserModel?> getUser();
  Future<bool> saveUser(UserModel user);
}
